import "./index.css";
import "./animista.css";

import { IObject } from "./interfaces";

import Alpine from "alpinejs";
import intersect from "@alpinejs/intersect";

import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
// import 'dayjs/locale/de'

import schedule from "./schedule.json";
import teams from "./teams.json";
import jury from "./jury.json";

dayjs.extend(duration);
// dayjs.locale('de')
dayjs().format();

const dayMap: IObject = {
  0: "Mo",
  1: "Di",
  2: "Mi",
  3: "Do",
  4: "Fr",
  5: "Sa",
  6: "So",
};

const app = () => {
  return {
    countDownDate: new Date(2022, 4, 10),
    countDownString: "0 Tage 00:00:00",
    teams: teams,
    schedule: schedule,
    jury: jury,
    selectedSchedule: 0,
    eventPosition: 0,
    showIntro: false,
    showModal: false,
    modalObject: {},
    events: [] as IObject[],
    dayjs: dayjs,
    async countDownTimer(): Promise<number> {
      const countdown = setInterval(() => {
        const x = dayjs(this.countDownDate);
        const y = dayjs();

        const duration = dayjs.duration(x.diff(y));

        this.countDownString = duration.format("M [Months] DD:HH:mm:ss");

        if (new Number(duration.format("ss")) < 0) {
          clearInterval(countdown);
          this.countDownString = "0 Monate 00:00:00:00";
        }
      }, 1000);
      return countdown;
    },
    calcEvents(): void {
      // typescript and access refs for alpinejs
      const self: any = this;
      const refs = self.$refs;
      const eventArray = [];

      // position of element rect
      const rect = refs.timeline.getBoundingClientRect();
      // const positions = {left:rect.x, right:rect.x+rect.width}
      const width = rect.width;

      // find selected event and copy to events array
      // const element: any = schedule.find(
      //   (element) => element.id === this.selectedSchedule
      // );

      for (const day of this.schedule) {
        // copy all objects to a new array
        const events = day.events.map((event: IObject) => event);
        // start and end time of day events
        const start = dayjs(
          `${dayjs(day.day).format("YYYY-MM-DD")} 08:00:00`
        );
        const end = dayjs(
          `${dayjs(day.day).format("YYYY-MM-DD")} 24:00:00`
        );

        // calculate day in minutes
        const full = dayjs(end).diff(start, "minutes", true);
        const newEvents = events.map((e: IObject) => {
          const x = dayjs(
            `${dayjs(day.day).format("YYYY-MM-DD")} ${e.from}`
          );
          const y = dayjs(
            `${dayjs(day.day).format("YYYY-MM-DD")} ${e.to}`
          );

          const diff = dayjs(y).diff(x, "minutes", true);

          const rightTime = dayjs(x);
          const mins = rightTime.diff(start, "minutes", true);

          const posPct = mins / full;
          const pct = diff / full;

          e["px"] = pct * width;
          e["pos"] = posPct * 100;
          e["from"] = x
          e["to"] = y

          return e;
        });

        eventArray.push(newEvents);
      }
      this.events = eventArray;
    },
    position(): void {
      const element: any = schedule.find(
        (element) => element.id === this.selectedSchedule
      );

      // start and end time of day events
      const start = dayjs(
        `${dayjs(element.day).format("YYYY-MM-DD")} 08:00:00`
      );
      const end = dayjs(
        `${dayjs(element.day).format("YYYY-MM-DD")} 24:00:00`
      );

      const full = dayjs(end).diff(start, "minutes", true);

      const today = dayjs();
      let diff = 0;

      if (today >= start) {
        diff = dayjs().diff(start, "minutes", true);
      }

      if (today >= end) {
        diff = full;
      }

      this.eventPosition = diff / full;
    },
    async positionInterval(): Promise<void> {
      setInterval(() => {
        this.position();
      }, 1000);
    },
    setDay(id: number): void {
      this.selectedSchedule = id;
    },
    formatDay(date: Date): any {
      return {
        day: dayjs(date).format("D"),
        mon: dayjs(date).format("MMM"),
        dayAbbr: dayMap[dayjs(date).day()],
      };
    },
    openModal(event: IObject): void {
      this.modalObject = event;
      this.showModal = true;
    },
    closeModal(): void {
      this.showModal = false;
    },
    init(): void {
      this.countDownTimer();
      this.calcEvents();
      this.positionInterval();
    },
  };
};

// now registering the alpine app
window.Alpine = Alpine;

Alpine.plugin(intersect);
Alpine.data("app", app);
Alpine.start();
